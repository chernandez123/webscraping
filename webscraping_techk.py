import requests
import csv
from bs4 import BeautifulSoup

class Editorial:
	def __init__(self):
		self.books = []
	def create_book(self, title, price, stock, category, cover):
		book = Book()
		book.title = title
		book.price = price
		book.stock = stock
		book.cover = cover
		book.category = category
		return book
	def create_product_description_to_book(self, upc, product_type, price_excl_tax, price_incl_tax, tax, availability, number_of_reviews, book):
		product_description = Product_Description()
		product_description.upc = upc
		product_description.product_type = product_type
		product_description.price_excl_tax = price_excl_tax
		product_description.price_incl_tax = price_incl_tax
		product_description.tax = tax
		product_description.availability = availability
		product_description.number_of_reviews = number_of_reviews
		book.product_description = product_description
		return book
	def add_book(self, book):
		self.books.append(book)
	def get_books(self):
		return self.books
	def show_books(self):
		for book in self.books:
			book.show()

class Book:
	def __init__(self):
		self.title = "-"
		self.price = 0
		self.stock = 0
		self.category = "-"
		self.cover = "-"
		self.product_description = Product_Description()
	def show(self):
		print (self.title)
		print (self.price)
		print (self.stock)
		print (self.category)
		self.product_description.show()

class Product_Description:
	def __init__(self):
		self.upc = "-"
		self.product_type = "-"
		self.price_excl_tax = 0
		self.price_incl_tax = 0
		self.tax = 0
		self.availability = 0
		self.number_of_reviews = 0
	def show(self):
		print (self.upc)
		print (self.product_type)
		print (self.price_excl_tax)
		print (self.price_incl_tax)
		print (self.tax)
		print (self.availability)
		print (self.number_of_reviews)

def get_category(tree_html_detail_book):
	book_categories = tree_html_detail_book.find_all("a");
	category = book_categories[3].text
	return category

def get_availability(availability_text):
	availability_text = availability_text.split(" ")[2]
	availability_count_stock = int(availability_text.split("(")[1])
	return availability_count_stock

editorial = Editorial()

url_catalog_results =  'http://books.toscrape.com/catalogue/page-1.html'
web_catalog_page_results = requests.get(url_catalog_results)
tree_html = BeautifulSoup(web_catalog_page_results.text, 'html.parser')

current_page = 1
components_page_results = tree_html.find_all(class_="current")
results = int(tree_html.find_all("strong")[0].text)
last_page = int(components_page_results[0].text.strip().split(" ")[3])

while current_page < last_page:
	url_catalog =  'http://books.toscrape.com/catalogue/page-'+str(current_page)+'.html'
	web_catalog_page = requests.get(url_catalog)
	tree_html = BeautifulSoup(web_catalog_page.text, 'html.parser')

	book_titles = tree_html.find_all(class_="thumbnail") #obtener todas las portadas de libros
	book_prices = tree_html.find_all(class_="price_color"); #precios
	book_stocks = tree_html.find_all(class_=["instock", "availability"]);

	url_book_title = tree_html.find_all("a");
	#Porque es el <a> n° 53 el primer producto del catálogo (por página). Los otros <a> son referencias a las categorías del <aside>
	index_url_book_title = 53 

	count_book_page_current = 0

	while count_book_page_current < len(book_titles):
		title = book_titles[count_book_page_current]["alt"]
		price = float(book_prices[count_book_page_current].text[2:])
		stock = book_stocks[count_book_page_current].text.strip()

		url_detail_book = 'http://books.toscrape.com/catalogue/'+ url_book_title[index_url_book_title]["href"]
		web_detail_book_page = requests.get(url_detail_book)
		tree_html_detail_book = BeautifulSoup(web_detail_book_page.text, 'html.parser')

		upc_detail_book = tree_html_detail_book.find_all("td")[0].text
		product_type_detail_book = tree_html_detail_book.find_all("td")[1].text
		price_excl_tax_detail_book = float(tree_html_detail_book.find_all("td")[2].text[2:])
		price_incl_tax_detail_book = float(tree_html_detail_book.find_all("td")[3].text[2:])
		tax_detail_book = float(tree_html_detail_book.find_all("td")[4].text[2:])
		availability_detail_book = get_availability(tree_html_detail_book.find_all("td")[5].text)
		number_of_reviews_detail_book = int(tree_html_detail_book.find_all("td")[6].text)

		category = get_category(tree_html_detail_book)
		cover = 'http://books.toscrape.com'+book_titles[count_book_page_current]["src"][2:]
		book = editorial.create_book(title, price, stock, category, cover)

		book = editorial.create_product_description_to_book(upc_detail_book, product_type_detail_book, price_excl_tax_detail_book, 
			price_incl_tax_detail_book, tax_detail_book, availability_detail_book, number_of_reviews_detail_book, book)

		editorial.add_book(book)
		count_book_page_current += 1
		#Se suma de 2 en 2 porque existen dos <a> por cada producto. Una referencia por la carátula y otra por el título.
		index_url_book_title += 2 
	print (current_page) #Estar al tanto de la página que se está haciendo scraping. Para no suponer que se pegó el programa.
	current_page += 1

attributes = [['Title', 'Price', 'Stock', 'Category', 'Cover', 
'UPC','Product Type', 'Price (excl. tax)', 'Price (incl. tax)', 'Tax', 'Availability', 'Number of reviews']]

with open ('books_csv.csv', 'w', newline='',encoding='utf-8') as csv_file:
	writer = csv.writer(csv_file)

	for book in editorial.get_books():
		values = []
		values.append(str(book.title))
		values.append(str(book.price))
		values.append(str(book.stock))
		values.append(str(book.category))
		values.append(str(book.cover))
		values.append(str(book.product_description.upc))
		values.append(str(book.product_description.product_type))
		values.append(str(book.product_description.price_excl_tax))
		values.append(str(book.product_description.price_incl_tax))
		values.append(str(book.product_description.tax))
		values.append(str(book.product_description.availability))
		values.append(str(book.product_description.number_of_reviews))
		attributes.append(values)
	writer.writerows(attributes)
	csv_file.close()
